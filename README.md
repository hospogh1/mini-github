# Mini GitHub

## Installation

Install `live-server` globally.

```bash
npm i -g live-server
# or
yarn global add live-server
```

## Run

```bash
npm run start
# or
yarn start
```
