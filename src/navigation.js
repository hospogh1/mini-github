
const {
  setupRouting,
  navigate,
} = (() => {
  'use strict';

  const ROUTES = {
    'users': usersPage,
    'user-detail': userDetail,
    'repos': reposPage,
  };
  const DEFAULT_ROUTE = '/users';
  let routerCallbackController;

  const navElem = document.querySelector('#root nav');
  const linkElemsMap = new Map();
  let linkElems;

  const canAccess = (path) => {
    const path0 = getPathItem(path);
    return Object.keys(ROUTES).some(k => k === path0);
  }

  const setActiveClass = (path) => {
    const selected = linkElemsMap.get(getPathItem(path));
    linkElems.forEach(elem => {
      if (elem === selected) {
        elem.classList.add('active');
      } else {
        elem.classList.remove('active');
      }
    });
  }

  const navigate = (path) => {
    if (!canAccess(window.location.pathname)) {
      history.replaceState(null, null, DEFAULT_ROUTE);
    } else {
      const url = `${window.location.origin}${path}${window.location.search}`
      window.history.pushState({}, path, url);
    }

    const newPath = getPathItem(window.location.pathname);
    setActiveClass(newPath);
    routerCallbackController(ROUTES[newPath]);
  };

  const setupRouting = (routerCallback) => {
    routerCallbackController = routerCallback;
    const origin = window.location.origin;
    linkElems = navElem.querySelectorAll('a');
    linkElems.forEach(anchor => {
      const path = anchor.href.split(origin)[1];
      anchor.href = 'javascript:void(0);';
      linkElemsMap.set(getPathItem(path), anchor);

      anchor.addEventListener('click', event => {
        event.preventDefault();
        navigate(path);
      });
    });
    navigate(window.location.pathname);
  }

  return {
    navigate,
    setupRouting,
  }
})();

