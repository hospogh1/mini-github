const reposPage = (() => {
  const ITEMS_PER_PAGE = 20;
  let currentSearchValue = '';
  let page = 1;
  let maxPage = 1;
  let prevButton;
  let nextButton;
  const container = createDiv()();

  const fetchRepos = async (searchText = '', page = 1) => {
    if (!searchText) {
      return Promise.resolve([]);
    }

    const data = await fetch(`https://api.github.com/search/repositories?perPage=20&q=${searchText}&page=${page}`);
    return data.json();
  };

  const getHeaderBar = (
    searchChangeHandler,
    prevHandler,
    nextHandler,
  ) => {
    const searchInput = createInput({
      placeholder: 'Search for Repos',
      handler: ({ target }) => searchChangeHandler(target.value),
    });

    prevButton = createPrevButton(prevHandler);
    nextButton = createNextButton(nextHandler);
    setButtonsActiveState();

    return createDiv('header-container')(
      searchInput,
      createDiv('pagination-buttons')(prevButton, nextButton),
    );
  };

  const createRepoRow = (data) => createDiv('repo')(
    createDiv('repo-user')(`Owner: ${data.owner.login}`),
    createDiv('name')(`Name: ${data.name}`),
    createDiv('lang')(`Language: ${data.language}`),
  );

  const setButtonsActiveState = () => {
    if (page === 1) {
      prevButton.setAttribute('disabled', 'true');
    } else {
      prevButton.removeAttribute('disabled');
    }

    if (page === maxPage) {
      nextButton.setAttribute('disabled', 'true');
    } else {
      nextButton.removeAttribute('disabled');
    }
  }
  const setQueryParams = () => {
    const queryString = obj2QueryParams({
      q: currentSearchValue,
      page,
    });

    const {origin, pathname} = window.location;
    const url = `${origin}${pathname}?${queryString}`;

    window.history.replaceState({}, '', url);
  }

  const fetchReposList = async() => {
    const { items, total_count } = await fetchRepos(currentSearchValue, page);
    const diff = total_count / ITEMS_PER_PAGE;
    maxPage = total_count % ITEMS_PER_PAGE ? diff : Math.floor(diff) + 1;

    return items || [];
  };

  const drawRepoRow = () => fetchReposList().then(repos => {
    setButtonsActiveState();
    container.innerHTML = '';
    multiAppend(container, repos.map(r => createRepoRow(r)));
  });

  const search = () => {
    setQueryParams();
    drawRepoRow();
  }

  const drawPage = () => {
    const headerBar = getHeaderBar(
      (searchValue) => {
        if ( searchValue !== currentSearchValue) {
          page = 1;
          maxPage = 1;
        }
        currentSearchValue = searchValue;
        search();
      },
      () => {
        if (page > 1) {
          page--;
        }
        search();
      },
      () => {
        if (page < maxPage) {
          page ++;
        }
        search();
      }
    );

    return createDiv()(
      headerBar,
      container,
    );
  }

  return (contentElem) => {
    contentElem.innerHTML = '';
    contentElem.appendChild(drawPage());
  };
})();
