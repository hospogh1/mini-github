(() => {
  'use strict';
  window.addEventListener('DOMContentLoaded', () => {
    const contentElem = document.getElementById('content');
    setupRouting(page => page(contentElem));
  });
})();
