const usersPage = (() => {
  const ITEMS_PER_PAGE = 20;
  let currentSearchValue = '';
  let page = 1;
  let maxPage = 1;
  const usersContainer = createDiv()();

  /** Returns users list array in promise. */
  async function fetchUsers(searchText = '', page = 1) {
    if (!searchText) {
      return Promise.resolve([]);
    }

    const data = await fetch(`https://api.github.com/search/users?perPage=20&q=${searchText}&page=${page}`);
    return data.json();
  }

  /** Starts search */
  function search() {
    setQueryParams();
    drawUsers();
  }

  // Pagination Buttons Handlers
  const prevHandler = () => {
    if (page > 1) {
      page--;
    }
    search();
  };
  const nextHandler = () => {
    if (page < maxPage) {
      page ++;
    }
    search();
  };
  const getPaginationButtons = () => createPaginationButtons(prevHandler, nextHandler);
  const searchChangeHandler = (searchValue) => {
    if ( searchValue !== currentSearchValue) {
      page = 1;
      maxPage = 1;
    }
    currentSearchValue = searchValue;
    search();
  };

  /**
   * Returns header bar which contains search-input
   * and pagination buttons.
   */
  function getHeaderBar() {
    const searchInput = createInput({
      placeholder: 'Search for users',
      handler: ({ target }) => searchChangeHandler(target.value),
    });

    const buttons = getPaginationButtons();
    setButtonsActiveState();

    return createDiv('header-container')(
      searchInput,
      buttons,
    );
  };

  /** Returns a user-card with filled data. */
  function createUserItem(data) {
    const userCard = createDiv('user')(
      createImg(data.avatar_url),
      createDiv('username')(data.login),
    );

    userCard.addEventListener('click', () => {
      navigate(`/user-detail?${obj2QueryParams(data)}`);
    });

    return userCard;
  }

  /** Sets correct enable/disable state for pagination buttons. */
  function setButtonsActiveState() {
    const prevButtons = document.querySelectorAll('.pagination-buttons .prev');
    const nextButtons = document.querySelectorAll('.pagination-buttons .next');

    if (page === 1) {
      prevButtons.forEach(button => button.setAttribute('disabled', 'true'));
    } else {
      prevButtons.forEach(button => button.removeAttribute('disabled'));
    }

    if (page === maxPage) {
      nextButtons.forEach(button => button.setAttribute('disabled', 'true'));
    } else {
      nextButtons.forEach(button => button.removeAttribute('disabled'));
    }
  }

  /** Sets search input values in url. */
  function setQueryParams() {
    const queryString = obj2QueryParams({
      q: currentSearchValue,
      page,
    });

    const {origin, pathname} = window.location;
    const url = `${origin}${pathname}?${queryString}`;

    window.history.replaceState({}, '', url);
  }

  /** Sets maxPage and returns users. */
  const getUsersList = async () => {
    const { items, total_count } = await fetchUsers(currentSearchValue, page);
    const diff = total_count / ITEMS_PER_PAGE;
    maxPage = total_count % ITEMS_PER_PAGE ? diff : Math.floor(diff) + 1;

    return items || [];
  };

  /** Inserts users cards in container. */
  const drawUsers = () => getUsersList().then(users => {
    setButtonsActiveState();
    usersContainer.innerHTML = '';
    multiAppend(usersContainer, users.map(u => createUserItem(u)));
  });

  /** Drawing starts here. */
  const drawPage = () => {
    const headerBar = getHeaderBar();

    return createDiv()(
      headerBar,
      usersContainer,
      getPaginationButtons(),
    );
  }

  return (contentElem) => {
    contentElem.innerHTML = '';
    contentElem.appendChild(drawPage());
  };
})();
