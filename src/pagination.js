/**
 * Returns a new prev-button for pagination.
 */
const createPrevButton = (clickHandler) => createButton(clickHandler, 'prev')(
  createIcon('arrow_right_alt'),
  'Prev',
);

/**
 * Returns a new next-button for pagination.
 */
const createNextButton = (clickHandler) => createButton(clickHandler, 'next')(
  'Next',
  createIcon('arrow_right_alt'),
);

/**
 * Returns a new pagination buttons-container.
 */
const createPaginationButtons = (prevHandler, nextHandler) => {
  const prev = createPrevButton(prevHandler);
  const next = createNextButton(nextHandler);

  return createDiv('pagination-buttons')(
    prev,
    next,
  );
}
