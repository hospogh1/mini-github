/// Helpers
const cleanPath = (hash) => hash.replace(/\//g, '');
const getPathItem = (path, itemIndex = 0) => {
  return path.replace(/^\//g, '').split('/')[itemIndex]
};
const multiAppend = (elem, children) => {
  children && children.forEach(child => elem.append(child));
};

/// Creators
const createElem = (elemName, classes = '') => (...children) => {
  const elem = document.createElement(elemName);
  elem.className = classes;
  multiAppend(elem, children);
  return elem;
};
const createDiv = (...classes) => createElem('div', classes.join(' '));
const createIcon = (name) => createElem('i', 'material-icons')(name);
const createButton = (handler, classes) => (...children) => {
  const button = createElem('button', classes)(...children);
  button.addEventListener('click', handler);

  return button;
}

const createInput = ({ placeholder, handler }) => {
  const searchInput = document.createElement('input');
  searchInput.setAttribute('type', 'text');
  searchInput.placeholder = placeholder;
  searchInput.addEventListener('change', handler);
  return searchInput;
};

const createImg = (src) => {
  const img = document.createElement('img');
  img.src = src;
  return img;
}

const obj2QueryParams = (obj) => {
  return Object.keys(obj)
  .map(k => `${k}=${encodeURIComponent(obj[k])}`)
  .join('&');
}

const queryParams2Obj = (str) => {
  const result = {};
  str.replace(/\?/g, '').split('&').map(item => {
    const parts = item.split('=');
    result[parts[0]] = decodeURIComponent(parts[1]);
  });
  return result;
}
